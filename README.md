Program: Words after "G"/"g"
Create a program inputs a phrase (like a famous quotation) and prints all of the words that start with h-z

Sample input:
enter a 1 sentence quote, non-alpha separate words: Wheresoever you go, go with all your heart

Sample output:

WHERESOEVER
YOU
WITH
YOUR
HEART